//heroku labs:enable websockets -a nodoso
//heroku config:add IP=chat-nodoso -a nodoso

//LIBRERIAS
var http = require('http');
var socketio = require("socket.io");
var express = require('express');
var ejs = require('ejs');

//---------------CONFIGURAMOS EXPRESS -----------------
var app = express();
var servidor = http.createServer(app);
//le indicamos cual es nuestra carpeta de recursos estaticos
app.use('/estatico', express.static(__dirname + '/estatico'));
//le indicamos que vamos a usar un sistema de templates basado en EJS
app.set('views', __dirname + '/views');
app.engine('.html', ejs.__express);
//le indicamos que el view engine sea con html
app.set('view engine', 'html');

//preparamos los puertos
var PUERTO = process.env.PORT || 5000;
PUERTO = parseInt(PUERTO);
//preparamos la ruta de socket.io
var URL_SOCKETIO = (process.env.PORT) ? process.env.HOST : "localhost" + ":" + PUERTO;

app.get("/pacman", function(req, res) {

	res.render("index", {
		socketURL : URL_SOCKETIO,
		jugador : "pacman"
	});
});

app.get("/fantasma", function(req, res) {

	res.render("index", {
		socketURL : URL_SOCKETIO,
		jugador : "fantasma"
	});
});

var io = socketio.listen(servidor);
//https://github.com/LearnBoost/Socket.IO/wiki/Configuring-Socket.IO
io.set("log level", 2);
//ARRANCAMOS EL SERVIDOR
servidor.listen(PUERTO);

var juegos = [];

//funcion auxiliar
function asignaJuego(socket) {
	var juego = null;
	//buscamos un juego vacio
	for (var numJuego = 0; numJuego < juegos.length; numJuego++) {

		if (juegos[numJuego].length < 2) {
			juego = numJuego;
		}
	}

	//si no hay juegos disponibles, creamos uno nuevo
	if (juego === null) {
		juegos.push([]);
		juego = juegos.length - 1;
	}

	//guardamos el socket de este juego
	juegos[juego].push(socket);
	//guardamos a nivel del socket una variable que se llama juego
	socket.set('juego', juego);

	//si la sala del juego ya esta llena
	if (juegos[juego].length == 2) {

		juegos[juego][0].set('contrincante', socket);
		juegos[juego][1].set('contrincante', juegos[juego][0]);
		//les indicamos que arranque el juego
		juegos[juego][0].emit('iniciar');
		juegos[juego][1].emit('iniciar');
	}

	return juego;
}

io.sockets.on("connection", function(socket) {

	var juego = asignaJuego(socket);

	socket.on('disconnect', function() {

		console.log("desconectando");

		socket.get('contrincante', function(err, contrincante) {

			if (contrincante) {
				contrincante.emit('end');
				contrincante.set("contrincante", null);
			}
		});

		socket.get('juego', function(err, juego) {
			var idx = juegos[juego].indexOf(socket);
			if (idx != -1) {
				juegos[juego].splice(idx, 1);
			}
		});
	});

	socket.on("movimiento", function(datos) {

		socket.get("contrincante", function(error, contrincante) {
			if (contrincante) {
				contrincante.volatile.emit("movimiento_remoto", datos);
			}
		});

	});
});

console.log("todo el servicio funcionando:");
