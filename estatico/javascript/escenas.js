Q.scene("nivel1", function(stage) {

	Q.state.set("puntaje", 0);

	var mapa = stage.collisionLayer(new Q.Mapa());
	mapa.configurar();

	stage.insert(new Q.UI.Text({
		label : "Puntaje",
		x : 500,
		y : 30,
		size : 20,
		color : "red",
		family : "Fascinate Inline"
	}));

	stage.insert(new Q.Puntaje({
		x : 500,
		y : 60
	}));

});

Q.scene("perdiste", function(stage) {

	var contenedor = stage.insert(new Q.UI.Container({
		x : Q.width / 2,
		y : Q.height / 2,
		fill : "rgba(0,0,0,0.5)"
	}));

	contenedor.insert(new Q.UI.Text({
		x : 10,
		y : -70,
		size : 60,
		color : "#FFEE00",
		label : "Game Over",
		family : "Fascinate Inline"
	}));

	var boton = contenedor.insert(new Q.UI.Button({
		x : 0,
		y : 0,
		//w : 300, //ancho
		//h : 400, //alto
		//asset : "mosaicos.png",
		//shadow : 10,
		//shadowColor : "rgba(0,0,0,0.5)", //solo para RGB, note que dice rgba
		fill : "red",
		fontColor : "white",
		label : "Volver a Jugar"
	}));

	boton.on("click", function() {

		Q.clearStages();
		//Q.stageScene("nivel1");
		iniciarJuego();
	});

});

//solucion ejercicio
Q.scene("intro", function(stage) {

	stage.insert(new Q.Sprite({
		x : 300,
		y : 100,
		asset : "pacman_titulo.png"
	}));

	var boton = stage.insert(new Q.UI.Button({
		x : 300,
		y : 200,
		fill : "#F9A11C",
		size : 40,
		fontColor : "white",
		label : "Comenzar a Jugar"
	}));

	boton.on("click", function() {

		Q.clearStages();
		Q.stageScene("nivel1", {
			sort : true
		});
	});

});
