//ESCUCHAMOS A LOS SOCKETS
//cambiar para heroku??
var socket = io.connect(location.origin);

Q.component("informarMovimiento", {

	//se llama cuando el componente es insertado
	added : function() {
		this.entity.on("step", this, "controles");
	},
	controles : function() {
		var p = this.entity.p;

		socket.emit("movimiento", {
			x : p.x,
			y : p.y
		});	
	}
});

Q.component("controlRemoto", {

	//se llama cuando el componente es insertado
	added : function() {

		var p = this.entity.p;

		socket.on("movimiento_remoto", function(datos) {
			//actualizamos la propiedad p
			p.x = datos.x;
			p.y = datos.y;
			//this.entity.p.step();
		});
	}
});

function iniciarJuego() {

	if (RECURSOS_LISTOS === true) {

		console.log("INICIANDO EN:" + TIPO_JUGADOR);
		//iniciamos la escena
		Q.stageScene("nivel1", {
			sort : true
		});

		var pacman = Q("Pacman").first();
		var fantasma = Q("Fantasma").first();

		if (TIPO_JUGADOR === "pacman") {

			pacman.add("controlLocal, informarMovimiento");
			fantasma.add("controlRemoto");
		} else {//es un fantasma

			pacman.add("controlRemoto");
			fantasma.add("controlLocal, informarMovimiento");
		}
	} else {
		//esperamos otro segundo
		setTimeout(iniciarJuego, 1000);
	}
}

socket.on("iniciar", function() {

	setTimeout(iniciarJuego, 1000);
});
