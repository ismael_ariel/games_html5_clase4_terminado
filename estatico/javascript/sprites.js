Q.animations("mosaicos_anim", {
	brillar : {
		frames : [27, 28],
		rate : 1 / 2
	},
	pacman : {
		frames : [70, 71],
		rate : 1 / 3
	}
});

//------------- PARA UI -----------
Q.UI.Text.extend("Puntaje", {
	init : function(p) {

		this._super(p, {
			label : "0",
			color : "white"
		});

		//ESCUCHAMOS
		//Q.state.on("change.puntaje", this, "actualizar");
	},
	actualizar : function(puntaje) {
		//aqui hay un bug
		this.p.label = puntaje + "";
	}
});

Q.Sprite.extend("Punto", {
	init : function(p) {

		this._super(p, {
			sheet : "mosaicos",
			sensor : true,
			type : PUNTO
		});

		this.on("sensor", function() {
			this.destroy();

			Q.state.inc("puntaje", 10);
			//Q.state.set("puntaje", Q.state.get("puntaje") + 10);
		});
	}
});

Q.Sprite.extend("Alimento", {
	init : function(p) {
		this._super(p, {
			sheet : "mosaicos",
			sensor : true,
			type : ALIMENTO
			//collisionMask : MASCARA_CAPA
		});

		this.on("sensor", function() {
			this.destroy();
		});
	}
});

//SOLUCIONES EJERCICIO
Q.Sprite.extend("Poder", {
	init : function(p) {
		this._super(p, {
			sheet : "mosaicos",
			sprite : "mosaicos_anim",
			type : PODER,
			sensor : true
		});

		this.on("sensor", function() {
			this.destroy();
		});

		this.add("animation");
		this.play("brillar");
	}
});

Q.Sprite.extend("Fantasma", {
	init : function(p) {
		this._super(p, {
			sheet : "mosaicos",
			type : ENEMIGO,
			z : 10,
			collisionMask : PACMAN | MAPA
		});

		this.add("2d");
	}
});

Q.Sprite.extend("Pacman", {
	init : function(p) {

		this._super(p, {
			sheet : "mosaicos",
			sprite : "mosaicos_anim",
			tileW : 18,
			tileH : 18,
			type : PACMAN,
			z : 20,
			velocidad : 100, //le pasamos directamente la velocidad
			collisionMask : ENEMIGO | ALIMENTO | PUNTO | PODER | MAPA
		});

		this.add("2d, animation");
		this.play("pacman");

		this.on("hit", function(colision) {

			//si es un punto
			if (colision.tile === 29) {
				//borramos el mosaico de la capa de colisiones
				colision.obj.setTile(colision.tileX, colision.tileY, -1);
			} else if (colision.obj.isA("Fantasma")) {

				//Q.clearStages();
				Q.stage(0).pause();
				Q.stageScene("perdiste", 1, {
					sort : true
				});
			}

		});
	}
});

Q.TileLayer.extend("Mapa", {
	init : function(p) {

		this._super({
			dataAsset : "mapa.tmx",
			sheet : "mosaicos",
			tileW : 20,
			tileH : 20,
			type : MAPA
			//collisionMask : MASCARA_CAPA
		});

	},
	configurar : function() {
		//obtenemos la matriz de mosaicos
		var matrizMosaicos = this.p.tiles;
		//iteramos sobre todos los renglones
		for (var renglon = 0; renglon < matrizMosaicos.length; renglon++) {

			//obtenemos un renglon en cada iteracion
			var renglonMosaicos = matrizMosaicos[renglon];
			//por cada renglon, iteramos todas sus columnas
			for (var columna = 0; columna < renglonMosaicos.length; columna++) {
				//obtenemos el numero del mosaico
				var numeroMosaico = renglonMosaicos[columna];

				//calculamos la posicion de este mosaico
				var posX = columna * 20 + 10;
				var posY = renglon * 20 + 10;
				var nombreClase = null;
				//si el mosaico esta vacio, insertamos un punto

				switch(numeroMosaico) {
					//alimentos
					case 90:
					//soluciones ejeercicio
					case 91:
					//soluciones ejeercicio
					case 92:
					//hamburguesa
					case 93:
						//papas
						nombreClase = "Alimento";
						break;
					case -1:
						nombreClase = "Punto";
						numeroMosaico = 29;
						break;
					case 70:
						nombreClase = "Pacman";
						break;
					//SOLUCIONES EJERCICIOS
					case 27:
						nombreClase = "Poder";
						break;
					case 30:
					//rojo
					case 40:
					//rosa
					case 50:
					//verde
					case 60:
						//naranja
						nombreClase = "Fantasma";
						break;
				}

				if (nombreClase != null) {

					//remplazamos el mosaico por un sprite
					var personaje = this.stage.insert(new Q[nombreClase]({
						x : posX,
						y : posY,
						sheet : "mosaicos",
						frame : numeroMosaico
					}));

					//de la matriz de colisiones borramos el mosaico original
					renglonMosaicos[columna] = -1;
				}
			}
		}
	}
});
